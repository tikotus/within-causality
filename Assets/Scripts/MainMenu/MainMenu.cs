﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

	string selected = "SOLO";

	// Use this for initialization
	void Start () {
		GameObject f = GameObject.Find ("SOLO");
		f.GetComponent<Animator> ().enabled = true;
	}
	
	// Update is called once per frame
	void Update () {

		if(Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S) || Input.GetAxis("D-pad Vertical") < 0 && selected == "SOLO")
        {
			selected = "CO-OP";

			GameObject findcontinue = GameObject.Find ("CO-OP");
			findcontinue.GetComponent<Animator> ().enabled = true;

			GameObject findnewgame = GameObject.Find ("SOLO");
			findnewgame.GetComponent<Animator> ().enabled = false;
		}

		if(Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W) || Input.GetAxis("D-pad Vertical") > 0 && selected == "CO-OP"){
			selected = "SOLO";

			GameObject findcontinue = GameObject.Find ("CO-OP");
			findcontinue.GetComponent<Animator> ().enabled = false;

			GameObject findnewgame = GameObject.Find ("SOLO");
			findnewgame.GetComponent<Animator> ().enabled = true;
		}

		if(Input.GetKey(KeyCode.Space) || Input.GetAxis("Right Trigger") > 0){
			if(selected == "SOLO")
            {
				SceneManager.LoadScene ("Solo");
			}
		}
	}
}
