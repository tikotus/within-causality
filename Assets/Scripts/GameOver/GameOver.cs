﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour {

	public GameObject newGame;
	public GameObject mainMenu;
	GameObject selected;

	// Use this for initialization
	void Start () {
		selected = newGame;
		newGame.GetComponent<Animator> ().enabled = true;

		GameObject findScore = GameObject.Find("Score");
		findScore.transform.position = new Vector3(-30,20,40);
		findScore.transform.eulerAngles = new Vector3(0,0,0);
	}

	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.Space) || Input.GetButtonDown("Button A")){
			GameObject deleteLastGameScore = GameObject.Find("Score");
			Destroy(deleteLastGameScore);
			SceneManager.LoadScene (selected == newGame ? "Solo" : "MainMenu");
		}
		else if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.DownArrow))
        {
			selected = selected == newGame ? mainMenu : newGame;
			mainMenu.GetComponent<Animator>().enabled = selected == mainMenu;
			newGame.GetComponent<Animator>().enabled = selected == newGame;
		}
	}
}
