﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sharpshot : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(gameObject.transform.position.z > 35){
			gameObject.transform.position += new Vector3(0,0,-30f * Time.deltaTime);
		}else if(gameObject.transform.position.z <= 35){
			GameObject load = GameObject.Find ("PlayerShip");

			gameObject.transform.LookAt (load.transform.position);
		}
	}
}
