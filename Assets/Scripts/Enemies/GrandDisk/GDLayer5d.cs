﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GDLayer5d : MonoBehaviour {

	public int HP = 10;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		gameObject.transform.Rotate (Vector3.up * 20 * Time.deltaTime);

		if(HP <= 0){
			GameObject load = GameObject.Find ("GDcore");
			GDCore loaded = load.GetComponent<GDCore> ();

			loaded.SectionsDestroyed++;

			Destroy (gameObject);
			Destroy (this);
		}
	}

	void OnCollisionEnter(Collision Col){
		if(Col.gameObject.name == "Laser"){
			HP--;

			//debris effect
			GameObject load = Resources.Load ("effects/Debris") as GameObject;
			GameObject loaded = Instantiate (load) as GameObject;

			loaded.gameObject.transform.position = Col.gameObject.transform.position;
		}
	}
}
