﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GDLayer4a : MonoBehaviour {

	public int HP = 10;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		gameObject.transform.Rotate (Vector3.down * 25 * Time.deltaTime);

		if(HP <= 0){
			GameObject load = GameObject.Find ("GDcore");
			GDCore loaded = load.GetComponent<GDCore> ();

			loaded.SectionsDestroyed++;

			Destroy (gameObject);
			Destroy (this);
		}
	}

	void OnCollisionEnter(Collision Col){
		GameObject load = GameObject.Find ("GDcore");
		GDCore loaded = load.GetComponent<GDCore> ();

		if(Col.gameObject.name == "Laser" && loaded.SectionsDestroyed >= 4){
			HP--;

			GameObject load2 = Resources.Load ("effects/Debris") as GameObject;
			GameObject loaded2 = Instantiate (load2) as GameObject;

			loaded2.gameObject.transform.position = Col.gameObject.transform.position;
		}
		else if(loaded.SectionsDestroyed < 4){

			GameObject load2 = Resources.Load ("effects/GDshield") as GameObject;
			GameObject loaded2 = Instantiate (load2) as GameObject;

			loaded2.gameObject.transform.position = Col.gameObject.transform.position;
		}
	}
}
