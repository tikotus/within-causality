﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GDcoreChargedProjectile : MonoBehaviour {

	float chargeTime = 0;
	float closingSpeed = 80f;
	float forwardSpeed = -20f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		chargeTime += Time.deltaTime;

		if(chargeTime >= 5){
			gameObject.transform.position += new Vector3 (0,0,forwardSpeed * Time.deltaTime);

			GameObject load = GameObject.Find ("PlayerShip");

			gameObject.transform.position = Vector3.MoveTowards (gameObject.transform.position, load.transform.position, closingSpeed * Time.deltaTime);

			closingSpeed -= Time.deltaTime;
			forwardSpeed -= Time.deltaTime;
		}
	}

	void OnCollisionEnter(Collision Col){
		if(Col.gameObject.name == "PlayerContainer"){
			Col.gameObject.GetComponent<Player> ().PlayerHP--;

			Destroy (gameObject);
			Destroy (this);
		}
	}
}
