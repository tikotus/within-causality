﻿using UnityEngine;
using System.Collections;

public class Cield : Enemy
{
    float stayLocation;
    float stayTimer;

    public int HP;
    public string MovementBehaviour;

    // Use this for initialization
    void Start()
    {
        HP = 1;

        name = "Cield";

        MovementBehaviour = "aimingPlayer";

        stayLocation = Random.Range(80,111);
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.Rotate(Random.Range(0, 6), Random.Range(0, 6), Random.Range(0, 6) * Time.deltaTime);

        //moving behaviour when far away
        if (gameObject.transform.position.z >= stayLocation)
        {
            gameObject.transform.position += new Vector3(0, 0, -100 * Time.deltaTime);
        }

        //movement behaviour when at staylocation + variations
        else if (gameObject.transform.position.z <= stayLocation)
        {
            if (MovementBehaviour == "aimingPlayer")
            {
                GameObject findplayership = GameObject.Find("PlayerShip");

                Vector3 lockZ = transform.position;
                lockZ.z = stayLocation;

                gameObject.transform.position = lockZ;

                gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, findplayership.transform.position, 80f * Time.deltaTime);
            }
            else if (MovementBehaviour == "forwardSlow")
            {
                gameObject.transform.position += new Vector3(0, 0, -20 * Time.deltaTime);
            }
            else if (MovementBehaviour == "stayAWhile")
            {
                if (stayTimer > 5)
                {
                    gameObject.transform.position += new Vector3(0, 0, -40 * Time.deltaTime);
                }
                stayTimer += Time.deltaTime;
            }
        }
        
        //HP sys
        if (HP <= 0)
        {
            //giving score to player
            GameObject load = GameObject.Find("Score");
            load.GetComponent<Scoredisplay>().Score += 15;

            //loading explosion effect
            GameObject loadExplosion = Resources.Load("effects/KubeExplosion") as GameObject;
            GameObject Explosion = Instantiate(loadExplosion) as GameObject;

            Explosion.transform.position = gameObject.transform.position;
            
            Destroy(gameObject);
        }
        
        //highlighting when aimline hits this enemy
        transform.GetChild(0).GetComponent<MeshRenderer>().enabled = false;

        if (highlighted)
        {
            transform.GetChild(0).GetComponent<MeshRenderer>().enabled = true;
        }

        highlighted = false;
    }
}
