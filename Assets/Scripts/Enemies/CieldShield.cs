﻿using UnityEngine;
using System.Collections;

public class CieldShield : MonoBehaviour
{
    float rechargeTimer;

    public int HP;

    // Use this for initialization
    void Start()
    {
        rechargeTimer = 15;
        HP = 30;

        name = "CieldShield";
    }

    // Update is called once per frame
    void Update()
    {
        
        if (HP <= 0)
        {
            GetComponent<SphereCollider>().enabled = false;
            GetComponent<MeshRenderer>().enabled = false;
            rechargeTimer -= Time.deltaTime;
        }

        if (rechargeTimer <= 0)
        {
            HP = 15;
            GetComponent<SphereCollider>().enabled = true;
            GetComponent<MeshRenderer>().enabled = true;
            rechargeTimer = 15;
        }
    }

    private void OnCollisionEnter(Collision col)
    {
        switch (col.transform.name)
        {
            //fellow enemies
            case "Disk":
                Physics.IgnoreCollision(col.transform.GetComponent<Collider>(), transform.GetComponent<Collider>());
                break;
            case "Kube":
                Physics.IgnoreCollision(col.transform.GetComponent<Collider>(), transform.GetComponent<Collider>());
                break;
            case "Schtryker":
                Physics.IgnoreCollision(col.transform.GetComponent<Collider>(), transform.GetComponent<Collider>());
                break;
            case "Skarab":
                Physics.IgnoreCollision(col.transform.GetComponent<Collider>(), transform.GetComponent<Collider>());
                break;
            case "VektronWing":
                Physics.IgnoreCollision(col.transform.GetComponent<Collider>(), transform.GetComponent<Collider>());
                break;
            //fellow enemy projectiles
            case "KubeProjectile":
                Physics.IgnoreCollision(col.transform.GetComponent<Collider>(), transform.GetComponent<Collider>());
                break;
            case "SkarabProjectile":
                Physics.IgnoreCollision(col.transform.GetComponent<Collider>(), transform.GetComponent<Collider>());
                break;
            case "VektronProjectile":
                Physics.IgnoreCollision(col.transform.GetComponent<Collider>(), transform.GetComponent<Collider>());
                break;
        }
    }
}
