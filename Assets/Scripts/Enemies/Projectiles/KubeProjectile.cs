﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KubeProjectile : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		gameObject.transform.Translate (Vector3.back * 110 * Time.deltaTime);

		if(gameObject.transform.position.z < -50){
			Destroy (gameObject);
			Destroy (this);
		}
	}

	void OnCollisionEnter(Collision col){
		if(col.gameObject.name == "PlayerContainer"){
			//damaging player
			col.gameObject.GetComponent<Player>().PlayerHP--;

			//shaking player camera
			GameObject _bringPlayerCamera = GameObject.Find ("Main Camera");
			_bringPlayerCamera.GetComponent<PlayerCamera> ().Shake = true;

			//creating projectile impact effect
			GameObject _bringKubeProjectileImpact = Resources.Load ("effects/KubeProjectileImpact") as GameObject;
			GameObject _KubeProjectileImpact = Instantiate (_bringKubeProjectileImpact) as GameObject;

			_KubeProjectileImpact.transform.position = gameObject.transform.position;

			Destroy (gameObject);
			Destroy (this);
		}
	}
}
