﻿using UnityEngine;
using System.Collections;

public class VektronProjectile : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.forward * 150 * Time.deltaTime);

        transform.Translate(Vector3.up * Random.Range(0, 61) * Time.deltaTime);
        transform.Translate(Vector3.down * Random.Range(0, 61) * Time.deltaTime);
        transform.Translate(Vector3.right * Random.Range(0, 61) * Time.deltaTime);
        transform.Translate(Vector3.left * Random.Range(0, 61) * Time.deltaTime);
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.name == "PlayerContainer"){
            col.transform.GetComponent<Player>().PlayerHP--;

            //shaking player camera
            GameObject _bringPlayerCamera = GameObject.Find("Main Camera");
            _bringPlayerCamera.GetComponent<PlayerCamera>().Shake = true;

            //creating projectile impact effect
            GameObject _bringKubeProjectileImpact = Resources.Load("effects/KubeProjectileImpact") as GameObject;
            GameObject _KubeProjectileImpact = Instantiate(_bringKubeProjectileImpact) as GameObject;

            _KubeProjectileImpact.transform.position = gameObject.transform.position;

            Destroy(gameObject);
        }
    }
}
