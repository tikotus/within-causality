﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disk : Enemy {

	float timerForDestruction = 10f;

	public int HP;

	// Use this for initialization
	void Start () {
		HP = 1;

		name = "Disk";

        //adding container as a parent for easier enemy mass destruction
        GameObject findparent = GameObject.Find("EnemyContainer");
        transform.parent = findparent.gameObject.transform;

    }
	
	// Update is called once per frame
	void Update () {
		//moving behaviour when far away
		if(gameObject.transform.position.z > 40){
			gameObject.transform.position += new Vector3 (0,0,-40f * Time.deltaTime);
			gameObject.transform.Rotate (0,0,5f);
		}
		//moving behaviour when close
		else if(gameObject.transform.position.z <= 40){
			GameObject findplayership = GameObject.Find ("PlayerShip");
			gameObject.transform.position = Vector3.MoveTowards (gameObject.transform.position, findplayership.transform.position, 20f * Time.deltaTime);
			gameObject.transform.Rotate (Random.Range(1,11),Random.Range(1,11),Random.Range(1,11));

			timerForDestruction -= Time.deltaTime;

            transform.GetComponent<TrailRenderer>().enabled = true;

			gameObject.transform.position += new Vector3 (0,0,-25f * Time.deltaTime);
		}
		//HP system
		if(HP <= 0){
			//giving score to player
			GameObject load = GameObject.Find("Score");
            load.GetComponent<Scoredisplay>().Score += 5;

			//bringing explosion effect
			GameObject _explosion = Resources.Load ("effects/DiskExplosion") as GameObject;
			GameObject explosion = Instantiate (_explosion) as GameObject;

			explosion.transform.position = gameObject.transform.position;

            GameObject ges = GameObject.Find("GeneralEventSystem");
            ges.GetComponent<GeneralEvents>().DropLoot(Random.Range(0, 101), "Disk", transform.position);

            Destroy (gameObject);
			Destroy (this);
		}
		if(timerForDestruction <= 0){
			//bringing explosion effect
			GameObject _explosion = Resources.Load ("effects/DiskExplosion") as GameObject;
			GameObject explosion = Instantiate (_explosion) as GameObject;

			explosion.transform.position = gameObject.transform.position;

			Destroy (gameObject);
			Destroy (this);
		}

        //highlighting when aimline hits this enemy

		transform.GetChild(0).GetComponent<MeshRenderer>().enabled = false;

		if (highlighted)
		{
			transform.GetChild(0).GetComponent<MeshRenderer>().enabled = true;
		}

		highlighted = false;
    }

	void OnCollisionEnter(Collision col){
		if(col.gameObject.name == "PlayerContainer"){
			//Player loses HP
			col.gameObject.GetComponent<Player>().PlayerHP--;

			//shaking player camera
			GameObject _cameraFind = GameObject.Find ("Main Camera");
			PlayerCamera _cameraScript = _cameraFind.GetComponent<PlayerCamera> ();

			_cameraScript.Shake = true;

			HP--;
		}
	}

}
