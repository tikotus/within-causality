﻿using UnityEngine;
using System.Collections;

public class VektronWing : MonoBehaviour
{
    float wingRotationTimer;

    public bool highlighted;
    public int HP;
    

    // Use this for initialization
    void Start()
    {
        highlighted = false;

        HP = 10;
        name = "VektronWing";
        
    }

    // Update is called once per frame
    void Update()
    {
        
        //handling wing movement
        switch (tag)
        {
            case "VektronWing1":
                if (wingRotationTimer <= 2)
                {
                    transform.Translate(Vector3.up * 1.5f * Time.deltaTime);
                }
                else if (wingRotationTimer > 2)
                {
                    transform.Translate(Vector3.down * 1.5f * Time.deltaTime);
                }

                wingRotationTimer += Time.deltaTime;
                break;

            case "VektronWing2":
                if (wingRotationTimer <= 2)
                {
                    transform.Translate(Vector3.left * 1.5f * Time.deltaTime);
                }
                else if (wingRotationTimer > 2)
                {
                    transform.Translate(Vector3.right * 1.5f * Time.deltaTime);
                }

                wingRotationTimer += Time.deltaTime;
                break;

            case "VektronWing3":
                if (wingRotationTimer <= 2)
                {
                    transform.Translate(Vector3.down * 1.5f * Time.deltaTime);
                }
                else if (wingRotationTimer > 2)
                {
                    transform.Translate(Vector3.up * 1.5f * Time.deltaTime);
                }

                wingRotationTimer += Time.deltaTime;
                break;

            case "VektronWing4":
                if (wingRotationTimer <= 2)
                {
                    transform.Translate(Vector3.right * 1.5f * Time.deltaTime);
                }
                else if (wingRotationTimer > 2)
                {
                    transform.Translate(Vector3.left * 1.5f * Time.deltaTime);
                }

                wingRotationTimer += Time.deltaTime;
                break;

        }
        //resetting wing movement timer
        if (wingRotationTimer >= 4)
        {
            FireProjectile();
            wingRotationTimer = 0;
        }

        //listening for HP
        if (HP <= 0)
        {
            GameObject load = Resources.Load("effects/VektronExplosion") as GameObject;
            GameObject loaded = Instantiate(load) as GameObject;
            switch (tag)
            {
                case "VektronWing1":
                    loaded.transform.position = transform.position;
                    loaded.transform.position += new Vector3(0, 3, 0);
                    break;
                case "VektronWing2":
                    loaded.transform.position = transform.position;
                    loaded.transform.position += new Vector3(-3, 0, 0);
                    break;
                case "VektronWing3":
                    loaded.transform.position = transform.position;
                    loaded.transform.position += new Vector3(0, -3, 0);
                    break;
                case "VektronWing4":
                    loaded.transform.position = transform.position;
                    loaded.transform.position += new Vector3(3, 0, 0);
                    break;
            }

            //dropping loot
            GameObject ges = GameObject.Find("GeneralEventSystem");
            ges.GetComponent<GeneralEvents>().DropLoot(Random.Range(0, 101), "vektronWing", transform.position);

            //giving score to player
            GameObject loadscore = GameObject.Find("Score");
            loadscore.GetComponent<Scoredisplay>().Score += 25;

            gameObject.SendMessageUpwards("WingDestroyed");
            Destroy(gameObject);
        }

        //highlighting when aimline hits this enemy
        transform.GetChild(0).GetComponent<MeshRenderer>().enabled = false;

        if (highlighted)
        {
            transform.GetChild(0).GetComponent<MeshRenderer>().enabled = true;
        }

        highlighted = false;
    }
    
    void MainBodyDestroyed()
    {
        GameObject load = Resources.Load("effects/KubeExplosion") as GameObject;
        GameObject loaded = Instantiate(load) as GameObject;

        loaded.transform.position = transform.position;

        Destroy(gameObject);
    }

    void FireProjectile()
    {
        GameObject load = Resources.Load("enemies/projectiles/VektronProjectile") as GameObject;
        GameObject loaded = Instantiate(load) as GameObject;

        loaded.transform.position = transform.position;
        loaded.transform.rotation = transform.rotation;
    }
}
