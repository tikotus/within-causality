﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Schtryker : Enemy {

	public int HP;

	bool selfBoost = false;

	float attackTimer = 20;
	float attackVelocity = -1;
	float rotateRate = 20;

    float stayLocation;

	// Use this for initialization
	void Start () {
		HP = 3;

        stayLocation = Random.Range(80,101);

		name = "Schtryker";

        //adding container as a parent for easier enemy mass destruction
        GameObject findparent = GameObject.Find("EnemyContainer");
        transform.parent = findparent.gameObject.transform;
    }
	
	// Update is called once per frame
	void Update () {
		GameObject findPlayerShip = GameObject.Find ("PlayerShip");
		gameObject.transform.Rotate (Vector3.forward * rotateRate * Time.deltaTime);

		//far away behaviour - closing in
		if(gameObject.transform.position.z > stayLocation)
        {
			gameObject.transform.position += new Vector3 (0,0,-120 * Time.deltaTime);
		}

		//up close behaviour - starting to rotate and countdown for attack
		if(gameObject.transform.position.z <= stayLocation)
        {
			rotateRate += 1f;
			attackTimer -= Time.deltaTime;
		}

		//targeting player when close and rotation not high enough
		if(rotateRate >= 500 && attackTimer > 0){
			gameObject.transform.position = Vector3.MoveTowards (gameObject.transform.position, findPlayerShip.transform.position, 100f * Time.deltaTime);

			if(selfBoost == false){
				HP += 10;
				selfBoost = true;
			}
		}

		//locking z position
		if(gameObject.transform.position.z <= stayLocation && attackTimer > 0){
			Vector3 lockZ = transform.position;
			lockZ.z = stayLocation;

			gameObject.transform.position = lockZ;
		}


		//trail effect from sudden acceleration
		if(attackTimer <= 0 && attackTimer >= -0.1){
			GameObject load = Resources.Load ("effects/SchtrykerAcceleration") as GameObject;
			GameObject loaded = Instantiate (load) as GameObject;

			loaded.transform.position = gameObject.transform.position;
		}
		//attacking
		if(attackTimer <= 0){
            gameObject.transform.Translate(Vector3.forward * 500 * Time.deltaTime);
            Destroy(gameObject, 10f * Time.deltaTime);
        }

		//removal
		if(HP <= 0){
            //giving score to player
            GameObject load = GameObject.Find("Score");
            load.GetComponent<Scoredisplay>().Score += 30;

            //explosion effect + removal
            GameObject load2 = Resources.Load ("effects/SchtrykerExplosion") as GameObject;
			GameObject loaded = Instantiate (load2) as GameObject;

			loaded.transform.position = gameObject.transform.position;

            GameObject ges = GameObject.Find("GeneralEventSystem");
            ges.GetComponent<GeneralEvents>().DropLoot(Random.Range(0, 101), "Schtryker", transform.position);

            Destroy (gameObject);
			Destroy (this);
		}
		if(gameObject.transform.position.z <= -30){
			//explosion effect + removal
			GameObject load = Resources.Load ("effects/SchtrykerExplosion") as GameObject;
			GameObject loaded = Instantiate (load) as GameObject;

			loaded.transform.position = gameObject.transform.position;

			Destroy (gameObject);
			Destroy (this);
		}


        //highlighting when aimline hits this enemy
        transform.GetChild(0).GetComponent<MeshRenderer>().enabled = false;

        if (highlighted)
        {
            transform.GetChild(0).GetComponent<MeshRenderer>().enabled = true;
        }

        highlighted = false;
    }

	void OnCollisionEnter(Collision col){
		if(col.gameObject.name == "PlayerContainer"){
			//Player loses HP
			col.gameObject.GetComponent<Player>().PlayerHP--;

			//shaking player camera
			GameObject _cameraFind = GameObject.Find ("Main Camera");
			PlayerCamera _cameraScript = _cameraFind.GetComponent<PlayerCamera> ();

			_cameraScript.Shake = true;

			//bringing explosion effect
			GameObject _explosion = Resources.Load ("effects/DiskExplosion") as GameObject;
			GameObject explosion = Instantiate (_explosion) as GameObject;

			explosion.transform.position = gameObject.transform.position;

			Destroy (gameObject);
			Destroy (this);
		}
	}
}
