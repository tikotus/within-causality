﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SectionLoop : MonoBehaviour {

	bool moveWithoutAnimation;

	float removeTimer;

	// Use this for initialization
	void Start () {
        removeTimer = 0;
        moveWithoutAnimation = false;
        
    }
	
	// Update is called once per frame
	void Update () {
		if(moveWithoutAnimation == true){
			removeTimer += Time.deltaTime;
			gameObject.transform.Translate (Vector3.up * 375 * Time.deltaTime);

			if(removeTimer >= 10){
				RemoveThis ();
			}
		}
	}

	void SpawnNext(){

        GameObject gs = GameObject.Find("GeneralEventSystem");
        gs.GetComponent<GeneralEvents>().sectionCount++;

        var getSectionCount = gs.GetComponent<GeneralEvents>().sectionCount;

        int randnumA = Random.Range(1, 37);
        int randnumB = Random.Range(1, 7);
        int sectionType = Random.Range(1, 6);

        if (sectionType >= 1 && sectionType <= 4)
        {
            GameObject load = Resources.Load("worldsections/Section" + Mathf.RoundToInt(randnumA) + "a") as GameObject;
            GameObject loaded = Instantiate(load) as GameObject;

            GameObject getScore = GameObject.Find("Score");

            //spawning enemies for type A sections
            switch (Random.Range(1, 29))
            {
                case 1:
                    if (getScore.GetComponent<Scoredisplay>().Score < 5500)
                    {
                        Spawn1Disk_RandomLoc();
                    }
                    else
                    {
                        Spawn30Disk_RandomLoc();
                    }
                    break;
                case 2:
                    if (getScore.GetComponent<Scoredisplay>().Score < 6500)
                    {
                        Spawn3Disk_RandomLoc();
                    }
                    else
                    {
                        Spawn50Disk_RandomLoc();
                    }
                    break;
                case 3:
                    Spawn5Disk_InRow_RandomLoc();
                    break;
                case 4:
                    Spawn10Disk_InRow_RandomLoc();
                    break;
                case 5:
                    if (getScore.GetComponent<Scoredisplay>().Score < 7500)
                    {
                        Spawn1Kube_RandomLoc();
                    }
                    else
                    {
                        Spawn30Kube_RandomLoc();
                    }
                    break;
                case 6:
                    Spawn3Kube_RandomLoc();
                    break;
                case 7:
                    SpawnKubeFormationWall_middle();
                    break;
                case 8:
                    SpawnKubeFormationWall_left();
                    break;
                case 9:
                    SpawnKubeFormationWall_right();
                    break;
                case 10:
                    Spawn1Schtryker_RandomLoc();
                    break;
                case 11:
                    Spawn3Schtryker_Randomloc();
                    break;
                case 12:
                    if (getSectionCount < 50)
                    {
                        Spawn1Kube_RandomLoc();
                    }
                    else if (getSectionCount > 50)
                    {
                        Spawn1Skarab_RandomLoc();
                    }
                    break;
                case 13:
                    Spawn1Vektron_RandomLoc();
                    break;
                case 14:
                    SpawnKubeFormationWall_up();
                    break;
                case 15:
                    SpawnKubeFormationWall_down();
                    break;
                case 16:
                    Spawn2Vektron_RandomLoc();
                    break;
                case 17:
                    Spawn3Vektron_RandomLoc();
                    break;
                case 18:
                    Spawn10Disk_RandomLoc();
                    break;
                case 19:
                    if (getScore.GetComponent<Scoredisplay>().Score < 3500)
                    {
                        Spawn10Disk_RandomLoc();
                    }
                    else
                    {
                        Spawn1Skarab_RandomLoc();
                    }
                    break;
                case 20:
                    Spawn10Kube_RandomLoc();
                    break;
                case 21:
                    SpawnKubeFormationX_middle();
                    break;
                case 22:
                    Spawn10Disk_RandomLoc();
                    break;
                case 23:
                    if (getScore.GetComponent<Scoredisplay>().Score < 3500)
                    {
                        Spawn3Kube_RandomLoc();
                    }
                    else
                    {
                        Spawn50Disk_RandomLoc();
                    }
                    break;
                case 24:
                    Spawn10Kube_RandomLoc();
                    break;
                case 25:
                    SpawnKubeFormationForwardslash_middle();
                    break;
                case 26:
                    SpawnKubeFormationBackslash_middle();
                    break;
                case 27:
                    Spawn30Disk_RandomLoc();
                    break;
                case 28:
                    Spawn30Disk_RandomLoc();
                    break;
            }
        }
        if (sectionType == 5)
        {
            GameObject load = Resources.Load("worldsections/Section" + Mathf.RoundToInt(randnumB) + "b") as GameObject;
            GameObject loaded = Instantiate(load) as GameObject;

            //enemies of the types B to D will be tied to the section prefabs
        }
    }

    //SPAWNING ENEMIES
    //DISK
    void Spawn1Disk_RandomLoc()
    {
        GameObject loadDisk = Resources.Load("enemies/Disk") as GameObject;
        GameObject Disk = Instantiate(loadDisk) as GameObject;

        Disk.transform.position = new Vector3(Random.Range(-25, 26), Random.Range(-25, 26), 600);
    }
    void Spawn3Disk_RandomLoc()
    {
        int num3 = 3;

        int posZ = 600;

        while (num3 > 0)
        {
            GameObject loadDisk = Resources.Load("enemies/Disk") as GameObject;
            GameObject Disk = Instantiate(loadDisk) as GameObject;

            Disk.transform.position = new Vector3(Random.Range(-25, 26), Random.Range(-25, 26), posZ);

            posZ += 5;
            num3--;
        }
    }
    void Spawn5Disk_InRow_RandomLoc(){
		int num5 = 5;

		int posX = Random.Range (-25, 26);
		int posY = Random.Range (-25, 26);
		int posZ = 600;

		while(num5 > 0){
			GameObject loadDisk = Resources.Load ("enemies/Disk") as GameObject;
			GameObject Disk = Instantiate (loadDisk) as GameObject;

			Disk.transform.position = new Vector3 (posX,posY,posZ);

			posZ += 5;
			num5--;
		}
	}
    void Spawn10Disk_RandomLoc()
    {
        int num10 = 10;

        int posX = Random.Range(-25, 26);
        int posY = Random.Range(-25, 26);
        int posZ = 600;

        while (num10 > 0)
        {
            GameObject loadDisk = Resources.Load("enemies/Disk") as GameObject;
            GameObject Disk = Instantiate(loadDisk) as GameObject;

            Disk.transform.position = new Vector3(posX, posY, posZ);

            posZ += 5;
            num10--;
        }
    }
	void Spawn10Disk_InRow_RandomLoc(){
		int num10 = 10;

		int posX = Random.Range (-25, 26);
		int posY = Random.Range (-25, 26);
		int posZ = 600;

		while(num10 > 0){
			GameObject loadDisk = Resources.Load ("enemies/Disk") as GameObject;
			GameObject Disk = Instantiate (loadDisk) as GameObject;

			Disk.transform.position = new Vector3 (posX,posY,posZ);

			posZ += 5;
			num10--;
		}
	}
    void Spawn30Disk_RandomLoc()
    {
        int num30 = 30;
        int zPos = 600;

        while (num30 > 0)
        {
            GameObject load = Resources.Load("enemies/Disk") as GameObject;
            GameObject loaded = Instantiate(load) as GameObject;

            loaded.transform.position = new Vector3(Random.Range(-25,26), Random.Range(-25, 26), zPos);

            zPos += 5;
            num30--;
        }
    }
    void Spawn50Disk_RandomLoc()
    {
        int num50 = 50;
        int zPos = 600;

        while (num50 > 0)
        {
            GameObject load = Resources.Load("enemies/Disk") as GameObject;
            GameObject loaded = Instantiate(load) as GameObject;

            loaded.transform.position = new Vector3(Random.Range(-25, 26), Random.Range(-25, 26), zPos);

            zPos += 10;
            num50--;
        }
    }

	//KUBE
	void Spawn1Kube_RandomLoc(){
		GameObject loadKube = Resources.Load ("enemies/Kube") as GameObject;
		GameObject Kube = Instantiate (loadKube) as GameObject;

        Kube.GetComponent<Kube>().MovementBehaviour = "aimingPlayer";

		Kube.transform.position = new Vector3 (Random.Range (-25,26), Random.Range(-25,26), 600);
	}
	void Spawn3Kube_RandomLoc(){
		int num3 = 3;
        int behNum = Random.Range(0,3);

		while (num3 > 0){
			GameObject loadKube = Resources.Load ("enemies/Kube") as GameObject;
			GameObject Kube = Instantiate (loadKube) as GameObject;

            Kube.transform.position = new Vector3 (Random.Range (-25,26), Random.Range(-25,26), 600);

            if (behNum == 0)
            {
                Kube.GetComponent<Kube>().MovementBehaviour = "forwardSlow";
            }
            else if (behNum == 1)
            {
                Kube.GetComponent<Kube>().MovementBehaviour = "stayAWhile";
            }
            else if (behNum == 2)
            {
                Kube.GetComponent<Kube>().MovementBehaviour = "aimingPlayer";
            }

			num3--;
		}
	}
    void Spawn10Kube_RandomLoc()
    {
        int num10 = 10;
        int behNum = Random.Range(0, 3);

        while (num10 > 0)
        {
            GameObject loadKube = Resources.Load("enemies/Kube") as GameObject;
            GameObject Kube = Instantiate(loadKube) as GameObject;

            Kube.transform.position = new Vector3(Random.Range(-25, 26), Random.Range(-25, 26), 600);

            if (behNum == 0)
            {
                Kube.GetComponent<Kube>().MovementBehaviour = "forwardSlow";
            }
            else if (behNum == 1)
            {
                Kube.GetComponent<Kube>().MovementBehaviour = "stayAWhile";
            }
            else if (behNum == 2)
            {
                Kube.GetComponent<Kube>().MovementBehaviour = "aimingPlayer";
            }
            num10--;
        }
    }

    void Spawn30Kube_RandomLoc()
    {
        int num30 = 30;
        int zPos = 600;

        while (num30 > 0)
        {
            GameObject load = Resources.Load("enemies/Kube") as GameObject;
            GameObject loaded = Instantiate(load) as GameObject;

            loaded.transform.position = new Vector3(Random.Range(-25, 26), Random.Range(-25, 26), zPos);

            zPos += 10;
            num30--;
        }
    }

    void SpawnKubeFormationWall_middle()
    {
        GameObject load = Resources.Load("enemies/formations/KubeFormationWall") as GameObject;
        GameObject loaded = Instantiate(load) as GameObject;
    }
    void SpawnKubeFormationWall_left()
    {
        GameObject load = Resources.Load("enemies/formations/KubeFormationWall") as GameObject;
        GameObject loaded = Instantiate(load) as GameObject;

        loaded.transform.position = new Vector3(-10, 0, loaded.transform.position.z);
    }
    void SpawnKubeFormationWall_right()
    {
        GameObject load = Resources.Load("enemies/formations/KubeFormationWall") as GameObject;
        GameObject loaded = Instantiate(load) as GameObject;

        loaded.transform.position = new Vector3(10, 0, loaded.transform.position.z);
    }
    void SpawnKubeFormationWall_up()
    {
        GameObject load = Resources.Load("enemies/formations/KubeFormationWall") as GameObject;
        GameObject loaded = Instantiate(load) as GameObject;

        loaded.transform.position = new Vector3(0, 10, loaded.transform.position.z);
    }
    void SpawnKubeFormationWall_down()
    {
        GameObject load = Resources.Load("enemies/formations/KubeFormationWall") as GameObject;
        GameObject loaded = Instantiate(load) as GameObject;

        loaded.transform.position = new Vector3(0, -10, loaded.transform.position.z);
    }

    void SpawnKubeFormationX_middle()
    {
        GameObject load = Resources.Load("enemies/formations/KubeFormationX") as GameObject;
        GameObject loaded = Instantiate(load) as GameObject;
    }
    void SpawnKubeFormationX_left()
    {
        GameObject load = Resources.Load("enemies/formations/KubeFormationX") as GameObject;
        GameObject loaded = Instantiate(load) as GameObject;

        loaded.transform.position = new Vector3(-10, 0, loaded.transform.position.z);
    }
    void SpawnKubeFormationX_right()
    {
        GameObject load = Resources.Load("enemies/formations/KubeFormationX") as GameObject;
        GameObject loaded = Instantiate(load) as GameObject;

        loaded.transform.position = new Vector3(10, 0, loaded.transform.position.z);
    }
    void SpawnKubeFormationX_up()
    {
        GameObject load = Resources.Load("enemies/formations/KubeFormationX") as GameObject;
        GameObject loaded = Instantiate(load) as GameObject;

        loaded.transform.position = new Vector3(0, 10, loaded.transform.position.z);
    }
    void SpawnKubeFormationX_down()
    {
        GameObject load = Resources.Load("enemies/formations/KubeFormationX") as GameObject;
        GameObject loaded = Instantiate(load) as GameObject;

        loaded.transform.position = new Vector3(0, -10, loaded.transform.position.z);
    }

    void SpawnKubeFormationForwardslash_middle()
    {
        GameObject load = Resources.Load("enemies/formations/KubeFormationForwardslash") as GameObject;
        GameObject loaded = Instantiate(load) as GameObject;
        
    }
    void SpawnKubeFormationBackslash_middle()
    {
        GameObject load = Resources.Load("enemies/formations/KubeFormationBackslash") as GameObject;
        GameObject loaded = Instantiate(load) as GameObject;
    }
    


    //SCHTRYKER
    void Spawn1Schtryker_RandomLoc()
    {
		GameObject loadSchtryker = Resources.Load ("enemies/Schtryker") as GameObject;
		GameObject Schtryker = Instantiate (loadSchtryker) as GameObject;

        Schtryker.transform.position = new Vector3(Random.Range(-25, 26), Random.Range(-25, 26), 600);
	}

    void Spawn3Schtryker_Randomloc()
    {
        int num3 = 3;

        while (num3 > 0)
        {
            GameObject loadSchtryker = Resources.Load("enemies/Schtryker") as GameObject;
            GameObject Schtryker = Instantiate(loadSchtryker) as GameObject;

            Schtryker.transform.position = new Vector3(Random.Range(-25, 26), Random.Range(-25, 26), 600);
            num3--;
        }
    }

    //SKARAB
    void Spawn1Skarab_RandomLoc()
    {
        GameObject loadSkarab = Resources.Load("enemies/Skarab") as GameObject;
        GameObject Skarab = Instantiate(loadSkarab) as GameObject;

        Skarab.transform.position = new Vector3 (Random.Range(-25,26), Random.Range(-25,26), 600);
    }

    void Spawn2Skarab_RandomLoc()
    {
        int num2 = 2;

        while (num2 > 0)
        {
            GameObject loadSkarab = Resources.Load("enemies/Skarab") as GameObject;
            GameObject Skarab = Instantiate(loadSkarab) as GameObject;

            Skarab.transform.position = new Vector3(Random.Range(-25, 26), Random.Range(-25, 26), 600);
            num2--;
        }
    }

    //VEKTRON
    void Spawn1Vektron_RandomLoc()
    {
        GameObject loadVektron = Resources.Load("enemies/Vektron") as GameObject;
        GameObject Vektron = Instantiate(loadVektron) as GameObject;

        Vektron.transform.position = new Vector3(Random.Range(-25,26), Random.Range(-25,26), 600);
    }

    void Spawn2Vektron_RandomLoc()
    {
        int num2 = 2;

        while (num2 > 0)
        {
            GameObject loadVektron = Resources.Load("enemies/Vektron") as GameObject;
            GameObject Vektron = Instantiate(loadVektron) as GameObject;

            Vektron.transform.position = new Vector3(Random.Range(-25, 26), Random.Range(-25, 26), 600);
            num2--;
        }
    }

    void Spawn3Vektron_RandomLoc()
    {
        int num3 = 3;

        while (num3 > 0)
        {
            GameObject loadVektron = Resources.Load("enemies/Vektron") as GameObject;
            GameObject Vektron = Instantiate(loadVektron) as GameObject;

            Vektron.transform.position = new Vector3(Random.Range(-25, 26), Random.Range(-25, 26), 600);

            num3--;
        }
    }


    void StopAnimation(){
		GetComponent<Animator>().enabled = false;
		moveWithoutAnimation = true;
	}

	void RemoveThis(){
		Destroy (gameObject);
	}
}
