﻿using UnityEngine;
using System.Collections;

public class DestroyAfter5f : MonoBehaviour
{
    float destructionTimer = 5f;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        destructionTimer -= Time.deltaTime;
        if (destructionTimer <= 0)
        {
            Destroy(gameObject);
            Destroy(this);
        }
    }
}
