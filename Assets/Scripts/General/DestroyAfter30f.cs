﻿using UnityEngine;
using System.Collections;

public class DestroyAfter30f : MonoBehaviour
{

    float destructionTimer = 30f;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        destructionTimer -= Time.deltaTime;
        if (destructionTimer <= 0)
        {
            Destroy(gameObject);
            Destroy(this);
        }
    }
}
