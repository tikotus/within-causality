﻿using UnityEngine;
using System.Collections;

public class MovementFlickering_25to56 : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.back * Random.Range(25, 56) * Time.deltaTime);
        transform.Translate(Vector3.forward * Random.Range(25, 56) * Time.deltaTime);
        transform.Translate(Vector3.right * Random.Range(25, 56) * Time.deltaTime);
        transform.Translate(Vector3.left * Random.Range(25, 56) * Time.deltaTime);
        transform.Translate(Vector3.up * Random.Range(25, 56) * Time.deltaTime);
        transform.Translate(Vector3.down * Random.Range(25, 56) * Time.deltaTime);
    }
}
