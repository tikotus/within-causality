﻿using UnityEngine;
using System.Collections;

public class DestroyAfter10f : MonoBehaviour
{

    float destructionTimer = 10f;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        destructionTimer -= Time.deltaTime;
        if (destructionTimer <= 0)
        {
            Destroy(gameObject);
            Destroy(this);
        }
    }
}
