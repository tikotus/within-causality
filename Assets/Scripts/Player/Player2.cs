﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player2 : MonoBehaviour {

    float wepCooldown;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Rotate(Vector3.forward * 100 * Time.deltaTime);
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Rotate(Vector3.back * 100 * Time.deltaTime);
        }

        if (Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            FireProjectile();
        }

        if (wepCooldown > 0)
        {
            wepCooldown -= Time.deltaTime;
        }
    }

    private void FireProjectile()
    {
        GameObject hc = GameObject.Find("heatCircle");
        hc.GetComponent<Image>().fillAmount += 0.02f;


    }
}
