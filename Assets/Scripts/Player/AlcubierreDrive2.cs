﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlcubierreDrive2 : MonoBehaviour {

	int rotationRate = 100;

	// Use this for initialization
	void Start () {
		
	}

	// Update is called once per frame
	void Update () {
		gameObject.transform.Rotate (Vector3.forward * rotationRate * Time.deltaTime);
		gameObject.transform.Rotate (Vector3.right * rotationRate/Random.Range(6,8) * Time.deltaTime);
		gameObject.transform.Rotate (Vector3.up * rotationRate/Random.Range(6,8) * Time.deltaTime);

		//for increasing rotationRate when player is evading
		GameObject player = GameObject.Find ("PlayerContainer");
		Player _pscrit = player.GetComponent<Player>();

		if(_pscrit.PlayerEvading == true){
			rotationRate = 500;
		}else if(_pscrit.PlayerEvading == false && rotationRate != 100){
			rotationRate = 100;
		}
	}
}
