﻿using UnityEngine;
using System.Collections;

public class PlayerMovementEdgeLine : MonoBehaviour
{
    
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        GameObject load = GameObject.Find("PlayerShip");

        //GetComponent<LineRenderer>().SetColors(new Color(0.9f,0.8f,0f,0.6f), new Color(0.9f, 0.8f, 0f, 0.6f));
        GetComponent<LineRenderer>().SetPosition(0, transform.position);
        GetComponent<LineRenderer>().SetPosition(1, load.transform.position);
        //GetComponent<LineRenderer>().SetWidth(0.4f,0.4f);
    }
}
