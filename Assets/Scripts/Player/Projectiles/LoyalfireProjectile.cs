﻿using UnityEngine;
using System.Collections;

public class LoyalfireProjectile : MonoBehaviour
{
    bool spawned = false;

    Vector3 lastPos;

    float maxRange = 51;
    float velocity = 25;

    // Use this for initialization
    void Start()
    {
        name = "Loyalfire";

        lastPos = transform.position;

        Destroy(gameObject, 30f);
        
    }

    // Update is called once per frame
    void Update()
    {
        velocity += Time.deltaTime;

        GameObject ps = GameObject.Find("PlayerShip");
        transform.position = new Vector3(ps.transform.position.x, ps.transform.position.y, transform.position.z);

        transform.Translate(Vector3.forward * velocity * Time.deltaTime);

        transform.Translate(Vector3.left * Random.Range(0, maxRange) * Time.deltaTime);
        transform.Translate(Vector3.right * Random.Range(0, maxRange) * Time.deltaTime);

        transform.Translate(Vector3.up * Random.Range(0, maxRange) * Time.deltaTime);
        transform.Translate(Vector3.down * Random.Range(0, maxRange) * Time.deltaTime);

        RaycastHit hit;

        Ray thisCol = new Ray(lastPos, transform.position);

        if (Physics.Raycast(thisCol, out hit, 1100))
        {
            switch(hit.collider.tag)
            {
                //ENEMIES
                case "KubeCollider":
					hit.collider.GetComponent<Kube>().HP--;
					Destroy(gameObject, 4f);
                    break;

                case "DiskCollider":
					hit.collider.GetComponent<Disk>().HP--;
					Destroy(gameObject, 4f);
                    break;

                case "SkarabCollider":
					hit.collider.GetComponent<Skarab>().HP--;
					Destroy(gameObject, 4f);
                    break;

                case "SchtrykerCollider":
					hit.collider.GetComponent<Schtryker>().HP--;
					Destroy(gameObject, 4f);
                    break;

                case "VektronWing1":
                    hit.collider.GetComponent<VektronWing>().HP--;
                    Destroy(gameObject, 4f);
                    break;
                case "VektronWing2":
                    hit.collider.GetComponent<VektronWing>().HP--;
                    Destroy(gameObject, 4f);
                    break;
                case "VektronWing3":
                    hit.collider.GetComponent<VektronWing>().HP--;
                    Destroy(gameObject, 4f);
                    break;
                case "VektronWing4":
                    hit.collider.GetComponent<VektronWing>().HP--;
                    Destroy(gameObject, 4f);
                    break;

                case "Cield":
                    hit.collider.GetComponent<Cield>().HP--;
                    Destroy(gameObject, 4f);
                    break;

                //PROJECTILES/MISC
                case "EnemyProjectile":
                    GameObject load = Resources.Load("effects/KubeProjectileImpact") as GameObject;
                    GameObject loaded = Instantiate(load) as GameObject;

                    loaded.transform.position = transform.position;

                    Destroy(hit.collider.gameObject);
                    break;

                case "CieldShield":
                    hit.collider.GetComponent<CieldShield>().HP -= 30;
                    Destroy(gameObject, 4f);
                    break;
                    
            }
        }

        lastPos = transform.position;
    }
}
