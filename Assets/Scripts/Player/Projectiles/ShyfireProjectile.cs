﻿using UnityEngine;
using System.Collections;

public class ShyfireProjectile : MonoBehaviour
{
    bool spawned = false;

    Vector3 lastPos;

    float maxRange = 31;

    // Use this for initialization
    void Start()
    {
        name = "Shyfire";

        lastPos = transform.position;

        Destroy(gameObject, 30f);
    }


    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.forward * 500 * Time.deltaTime);

        transform.Translate(Vector3.left * Random.Range(0, maxRange) * Time.deltaTime);
        transform.Translate(Vector3.right * Random.Range(0, maxRange) * Time.deltaTime);

        transform.Translate(Vector3.up * Random.Range(0, maxRange) * Time.deltaTime);
        transform.Translate(Vector3.down * Random.Range(0, maxRange) * Time.deltaTime);

        transform.Translate(Vector3.forward * Random.Range(0, maxRange) * Time.deltaTime);
        transform.Translate(Vector3.back * Random.Range(0, maxRange) * Time.deltaTime);

        RaycastHit hit;

        Ray thisCol = new Ray(lastPos, transform.position);

        if (Physics.Raycast(thisCol, out hit, 1100))
        {
            GameObject load = Resources.Load("effects/ShyfireImpact") as GameObject;
            GameObject loaded = Instantiate(load) as GameObject;

            loaded.transform.position = transform.position;

            switch(hit.collider.tag)
            {
                //ENEMIES
                case "KubeCollider":
					hit.collider.GetComponent<Kube>().HP -= 10;
					maxRange += 600;
                    break;
                case "DiskCollider":
					hit.collider.GetComponent<Disk>().HP -= 10;
					maxRange += 600;
                    break;
                case "SkarabCollider":
					hit.collider.GetComponent<Skarab>().HP -= 10;
					maxRange += 600;
                    break;
                case "SchtrykerCollider":
					hit.collider.GetComponent<Schtryker>().HP -= 10;
					maxRange += 600;
                    break;
                case "VektronWing1":
                    hit.collider.GetComponent<VektronWing>().HP -= 10;
                    maxRange += 600;
                    break;
                case "VektronWing2":
                    hit.collider.GetComponent<VektronWing>().HP -= 10;
                    maxRange += 600;
                    break;
                case "VektronWing3":
                    hit.collider.GetComponent<VektronWing>().HP -= 10;
                    maxRange += 600;
                    break;
                case "VektronWing4":
                    hit.collider.GetComponent<VektronWing>().HP -= 10;
                    maxRange += 600;
                    break;

                //PROJECTILES / MISC
                case "EnemyProjectile":
					GameObject load1 = Resources.Load("effects/KubeProjectileImpact") as GameObject;
					GameObject loaded1 = Instantiate(load1) as GameObject;

					loaded1.transform.position = transform.position;

					Destroy(hit.collider.gameObject);
                    break;

            }
        }

        lastPos = transform.position;
    }
}
