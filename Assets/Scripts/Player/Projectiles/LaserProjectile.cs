﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserProjectile : MonoBehaviour {

	float velocity = 100;

	// Use this for initialization
	void Start () {
		name = "Laser";

		//spawning muzzle flash
		GameObject load = Resources.Load ("effects/LaserMuzzleFlash") as GameObject;
		GameObject loaded = Instantiate (load) as GameObject;

		loaded.transform.position = gameObject.transform.position;

        //playing firing sound
        int randSound = Random.Range(1,5);
        if (randSound == 1)
        {
            AudioClip clip = (AudioClip) Resources.Load("sounds/player/laser/laserVariant1");
            transform.GetComponent<AudioSource>().PlayOneShot(clip);
        }
        else if (randSound == 2)
        {
            AudioClip clip = (AudioClip)Resources.Load("sounds/player/laser/laserVariant2");
            transform.GetComponent<AudioSource>().PlayOneShot(clip);
        }
        else if (randSound == 3)
        {
            AudioClip clip = (AudioClip)Resources.Load("sounds/player/laser/laserVariant3");
            transform.GetComponent<AudioSource>().PlayOneShot(clip);
        }
        else if (randSound == 4)
        {
            AudioClip clip = (AudioClip)Resources.Load("sounds/player/laser/laserVariant4");
            transform.GetComponent<AudioSource>().PlayOneShot(clip);
        }
    }

	// Update is called once per frame
	void Update () {
		transform.Translate (Vector3.forward * velocity * Time.deltaTime);
		velocity += 15;

		if(gameObject.transform.position.z > 500){
			Destroy (gameObject);
			Destroy (this);
		}
	}

	//Normal enemy collisions (damage etc.) and general collision impact effect and self destruction
	//no Boss collisions or effects here, they are done independently by Bosses and their parts.
	void OnCollisionEnter(Collision col){

        switch(col.gameObject.name)
        {
            
            case "Disk":
				//damaging Disk
				col.gameObject.GetComponent<Disk>().HP--;
				Destroy(gameObject);
                break;
            case "Kube":
				//damaging Kube
				col.gameObject.GetComponent<Kube>().HP--;
				Destroy(gameObject);
                break;
            case "Schtryker":
				//damaging Schtryker
				col.gameObject.GetComponent<Schtryker>().HP--;
				Destroy(gameObject);
                break;
            case "Skarab":
                //damaging skarab
				col.gameObject.GetComponent<Skarab>().HP--;
				Destroy(gameObject);
                break;
            case "VektronWing":
                //damaging vektronWing (and thus Vektron)
                col.gameObject.GetComponent<VektronWing>().HP--;
                Destroy(gameObject);
                break;
            //projectile interceptions
            case "Cield":
                //damaging cield
                col.gameObject.GetComponent<Cield>().HP--;
                Destroy(gameObject);
                break;
            case "KubeProjectile":
				//loading kubeprojectile impact effect
				GameObject load = Resources.Load("effects/KubeProjectileImpact") as GameObject;
				GameObject loaded = Instantiate(load) as GameObject;

				loaded.transform.position = gameObject.transform.position;

				Destroy(col.gameObject);
                Destroy(gameObject);
                break;
            case "SkarabProjectile":
                //loading kubeprojectile impact effect
                GameObject load1 = Resources.Load("effects/KubeProjectileImpact") as GameObject;
                GameObject loaded1 = Instantiate(load1) as GameObject;

                loaded1.transform.position = gameObject.transform.position;

                Destroy(col.gameObject);
                Destroy(gameObject);
                break;
            case "VektronProjectile":
                //loading kubeprojectile impact effect
                GameObject load2 = Resources.Load("effects/KubeProjectileImpact") as GameObject;
                GameObject loaded2 = Instantiate(load2) as GameObject;

                loaded2.transform.position = gameObject.transform.position;

                Destroy(col.gameObject);
                Destroy(gameObject);
                break;
            case "CieldShield":
                //damaging cields shield
                col.gameObject.GetComponent<CieldShield>().HP--;
                Destroy(gameObject);
                break;
        }

		//making impact effect for laser projectile
		GameObject _loadLaserImpact = Resources.Load("effects/LaserImpact") as GameObject;
		GameObject _LaserImpact = Instantiate(_loadLaserImpact) as GameObject;

		_LaserImpact.transform.position = gameObject.transform.position;
        
	}

}