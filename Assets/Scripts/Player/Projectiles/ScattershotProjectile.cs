﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScattershotProjectile : MonoBehaviour {

	float velocity = 150;

	// Use this for initialization
	void Start () {
		name = "Scattershot";
	}
	
	// Update is called once per frame
	void Update () {
		gameObject.transform.Translate (Vector3.forward * velocity * Time.deltaTime);

		gameObject.transform.Translate (Vector3.up * Random.Range (20, 91) * Time.deltaTime);
		gameObject.transform.Translate (Vector3.down * Random.Range (20, 91) * Time.deltaTime);
		gameObject.transform.Translate (Vector3.left * Random.Range (20, 91) * Time.deltaTime);
		gameObject.transform.Translate (Vector3.right * Random.Range (20, 91) * Time.deltaTime);

		gameObject.transform.Rotate (Vector3.right * Random.Range (20, 31) * Time.deltaTime);
		gameObject.transform.Rotate (Vector3.left * Random.Range (20, 31) * Time.deltaTime);


		velocity += 100 * Time.deltaTime;

		if(gameObject.transform.position.z >= 500){
			Destroy (gameObject);
			Destroy (this);
		}
	}

	void OnCollisionEnter(Collision col){

        switch(col.gameObject.name)
        {
            //ENEMIES
            case "Disk":
                col.gameObject.GetComponent<Disk>().HP -= 2;
                Destroy(gameObject, 1f);
                break;
            case "Kube":
                col.gameObject.GetComponent<Kube>().HP -= 2;
                Destroy(gameObject, 1f);
                break;
            case "Schtryker":
                col.gameObject.GetComponent<Schtryker>().HP -= 2;
                Destroy(gameObject, 1f);
                break;
            case "Skarab":
                col.gameObject.GetComponent<Skarab>().HP -= 2;
                Destroy(gameObject, 1f);
                break;
            case "VektronWing":
                col.gameObject.GetComponent<VektronWing>().HP -= 2;
                Destroy(gameObject, 1f);
                break;
            case "Cield":
                col.gameObject.GetComponent<Cield>().HP -= 2;
                Destroy(gameObject, 1f);
                break;

                //PROJECTILES/MISC
            case "KubeProjectile":
				//loading kubeprojectile impact effect
				GameObject load1 = Resources.Load("effects/KubeProjectileImpact") as GameObject;
				GameObject loaded1 = Instantiate(load1) as GameObject;

				loaded1.transform.position = transform.position;

				Destroy(col.gameObject);
                Destroy(gameObject, 1f);
                break;
            case "SkarabProjectile":
                //loading kubeprojectile impact effect
                GameObject load2 = Resources.Load("effects/KubeProjectileImpact") as GameObject;
                GameObject loaded2 = Instantiate(load2) as GameObject;

                loaded2.transform.position = transform.position;

                Destroy(col.gameObject);
                Destroy(gameObject, 1f);
                break;
            case "VektronProjectile":
                //loading kubeprojectile impact effect
                GameObject load3 = Resources.Load("effects/KubeProjectileImpact") as GameObject;
                GameObject loaded3 = Instantiate(load3) as GameObject;

                loaded3.transform.position = transform.position;

                Destroy(col.gameObject);
                Destroy(gameObject, 1f);
                break;
        }

		GameObject load = Resources.Load ("effects/ScattershotImpact") as GameObject;
		GameObject loaded = Instantiate (load) as GameObject;

		loaded.transform.position = gameObject.transform.position;
	}
}
