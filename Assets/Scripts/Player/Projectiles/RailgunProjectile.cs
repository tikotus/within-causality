﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RailgunProjectile : MonoBehaviour {

    public float speed;

	// Use this for initialization
	void Start () {

        speed = 0;
    }

	// Update is called once per frame
	void Update () {
        transform.Translate(Vector3.forward * speed * Time.deltaTime);
        speed += 1000 * Time.deltaTime;

        if (transform.position.z > 1600)
        {
            Destroy(gameObject);
        }
    }

	void OnCollisionEnter (Collision col) {
        switch(col.gameObject.name)
        {
            //ENEMIES
            case "Kube":
                col.gameObject.GetComponent<Kube>().HP -= 50;
                break;
            case "Disk":
                col.gameObject.GetComponent<Disk>().HP -= 50;
                break;
            case "Schtryker":
                col.gameObject.GetComponent<Schtryker>().HP -= 50;
                break;
            case "Skarab":
                col.gameObject.GetComponent<Skarab>().HP -= 50;
                break;
            case "VektronWing":
                col.gameObject.GetComponent<VektronWing>().HP -= 50;
                break;
            case "Cield":
                col.gameObject.GetComponent<Cield>().HP -= 50;
                break;

            //PROJECTILES/MISC
            case "KubeProjectile":
				GameObject load2 = Resources.Load("effects/KubeProjectileImpact") as GameObject;
				GameObject loaded2 = Instantiate(load2) as GameObject;

                loaded2.gameObject.transform.position = col.gameObject.transform.position;

                Destroy(col.gameObject);
                break;

            case "SkarabProjectile":
                GameObject load3 = Resources.Load("effects/KubeProjectileImpact") as GameObject;
                GameObject loaded3 = Instantiate(load3) as GameObject;

                loaded3.gameObject.transform.position = col.gameObject.transform.position;

                Destroy(col.gameObject);
                break;

            case "VektronProjectile":
                GameObject load4 = Resources.Load("effects/KubeProjectileImpact") as GameObject;
                GameObject loaded4 = Instantiate(load4) as GameObject;

                loaded4.gameObject.transform.position = col.gameObject.transform.position;

                Destroy(col.gameObject);
                break;

            case "CieldShield":
                col.gameObject.GetComponent<CieldShield>().HP -= 50;
                break;
        }

		GameObject load = Resources.Load ("effects/RailgunImpact") as GameObject;
		GameObject loaded = Instantiate (load) as GameObject;

		loaded.transform.position = col.transform.position;
		loaded.transform.rotation = gameObject.transform.rotation;
	}
}
