﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UI_health : MonoBehaviour
{

    public int shownHP;

    List<GameObject> barList = new List<GameObject>();

    // Use this for initialization
    void Start()
    {
        shownHP = 20;

        barList.Add(transform.GetChild(0).gameObject);
        barList.Add(transform.GetChild(1).gameObject);
        barList.Add(transform.GetChild(2).gameObject);
        barList.Add(transform.GetChild(3).gameObject);
        barList.Add(transform.GetChild(4).gameObject);
        barList.Add(transform.GetChild(5).gameObject);
        barList.Add(transform.GetChild(6).gameObject);
        barList.Add(transform.GetChild(7).gameObject);
        barList.Add(transform.GetChild(8).gameObject);
        barList.Add(transform.GetChild(9).gameObject);
        barList.Add(transform.GetChild(10).gameObject);
        barList.Add(transform.GetChild(11).gameObject);
        barList.Add(transform.GetChild(12).gameObject);
        barList.Add(transform.GetChild(13).gameObject);
        barList.Add(transform.GetChild(14).gameObject);
        barList.Add(transform.GetChild(15).gameObject);
        barList.Add(transform.GetChild(16).gameObject);
        barList.Add(transform.GetChild(17).gameObject);
        barList.Add(transform.GetChild(18).gameObject);
        barList.Add(transform.GetChild(19).gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        GameObject fp = GameObject.Find("PlayerContainer");
        var ps = fp.GetComponent<Player>();


        //checking playerHP and updating shown HP bars
        if (shownHP != ps.PlayerHP)
        {
            foreach (GameObject go in barList)
            {
                if (int.Parse(go.gameObject.name) > ps.PlayerHP)
                {
                    go.GetComponent<MeshRenderer>().enabled = false;
                }
                if (int.Parse(go.gameObject.name) <= ps.PlayerHP)
                {
                    go.GetComponent<MeshRenderer>().enabled = true;
                    shownHP = ps.PlayerHP;
                }
            }
        }


    }
}
