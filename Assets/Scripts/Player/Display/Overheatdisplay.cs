﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Overheatdisplay : MonoBehaviour
{

    //public Material SkyboxW;
    //public Material SkyboxB;

           
    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        //Brings playerscript for editing textmesh tied to player
        GameObject load = GameObject.Find("PlayerContainer");
        Player ps = load.GetComponent<Player>();

        if (GetComponent<Image>().fillAmount != 0)
        {
            GetComponent<Image>().fillAmount -= 0.0005f;
            if (ps.ReleasingHeat == true)
            {
                GetComponent<Image>().fillAmount -= 0.0015f;
            }
        }
    }
}
