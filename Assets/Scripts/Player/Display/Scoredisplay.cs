﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Scoredisplay : MonoBehaviour {

	public Material SkyboxW;
	public Material SkyboxB;

    public int Score;

	// Use this for initialization
	void Start () {
        Score = 0;
	}
	
	// Update is called once per frame
	void Update () {

        //accessing text mesh component to update current score display
        GetComponent<TextMesh>().text = "SCORE: " + Score;

        //changing hp display color according to skybox
        if (RenderSettings.skybox == SkyboxW && GetComponent<TextMesh>().color != Color.black) {
            GetComponent<TextMesh>().color = Color.black;
        } else if (RenderSettings.skybox == SkyboxB && GetComponent<TextMesh>().color != Color.white) {
            GetComponent<TextMesh>().color = Color.white;
        }
	}

    void gameOver()
    {
        transform.position = new Vector3(0,0,0);
    }
}
