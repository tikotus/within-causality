﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class WeaponCooldownDisplay : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        GetComponent<Image>().fillAmount = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<Image>().fillAmount != 0)
        {
            GetComponent<Image>().fillAmount -= 0.010f;
        }
    }
}
