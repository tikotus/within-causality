﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crosshair : MonoBehaviour {

	public Material SkyboxW;
	public Material SkyboxB;

    public bool lineObscrured;

	// Use this for initialization
	void Start () {
    }
	
	// Update is called once per frame
	void Update () {

        GameObject ps = GameObject.Find ("PlayerShip");
		GameObject load = GameObject.Find ("PlayerContainer");
		Player pscript = load.GetComponent<Player> ();
        
        if (!lineObscrured)
        {
            var aimLine = GetComponent<LineRenderer>();
			aimLine.startColor = new Color(0, 0, 0, 0.5f);
            aimLine.endColor = new Color(0, 0, 0, 0.5f);
			aimLine.startWidth = 0.1f;
			aimLine.endWidth = 0.1f;
            aimLine.SetPosition(0, gameObject.transform.position);
            aimLine.SetPosition(1, ps.transform.position);

            //locking crosshair to mouse position
            if (pscript.PlayerMouseAiming == true) {
                Cursor.visible = false;

                Vector3 mousePos = Input.mousePosition;
                mousePos.z = transform.position.z - Camera.main.transform.position.z;

                gameObject.transform.position = Camera.main.ScreenToWorldPoint(mousePos);

                transform.LookAt(ps.transform.position);
            } else if (pscript.PlayerMouseAiming == false) {
                Cursor.visible = true;

                gameObject.transform.position = new Vector3(ps.gameObject.transform.position.x, ps.gameObject.transform.position.y, 600f);
            }
        }

        if (pscript.PlayerMouseAiming == false && pscript.PlayerControllerAiming == false)
        {
            transform.position = ps.transform.position;
            transform.position += new Vector3(0, 0, 600);
        }

        lineObscrured = false;

		//changing crosshair color according to skybox
		if(RenderSettings.skybox == SkyboxW && GetComponent<TextMesh> ().color != Color.black){
			GetComponent<TextMesh> ().color = Color.black;
		}else if(RenderSettings.skybox == SkyboxB && GetComponent<TextMesh> ().color != Color.white){
			GetComponent<TextMesh> ().color = Color.white;
		}
	}

    private void OnCollisionStay()
    {
        Color hitC = new Color(1f,0,0,0.5f);
        float hitW = 0.6f;

		transform.GetComponent<LineRenderer>().startColor = hitC;
		transform.GetComponent<LineRenderer>().endColor = hitC;
		transform.GetComponent<LineRenderer>().startWidth = hitW;
		transform.GetComponent<LineRenderer>().endWidth = hitW;
    }

    private void OnCollisionExit()
    {
        Color nothitC = new Color(0, 0, 0, 0.5f);
        float nothitW = 0.5f;

		transform.GetComponent<LineRenderer>().startColor = nothitC;
		transform.GetComponent<LineRenderer>().endColor = nothitC;
		transform.GetComponent<LineRenderer>().startWidth = nothitW;
		transform.GetComponent<LineRenderer>().endWidth = nothitW;
    }

}
