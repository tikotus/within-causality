﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupBoxHP : MonoBehaviour {

	// Use this for initialization
	void Start () {
        name = "PickupBoxHP";

        transform.rotation = Random.rotation;
    }

	// Update is called once per frame
	void Update () {
        GameObject ps = GameObject.Find("PlayerContainer");

		transform.position += new Vector3 (0,0,-0.6f);
        transform.Rotate(Vector3.up * 500 * Time.deltaTime);


        //moving to player if player is close enough
        if (transform.position.z > ps.transform.position.z - 20 && transform.position.z < ps.transform.position.z + 20)
        {
            if (transform.position.x > ps.transform.position.x - 20 && transform.position.x < ps.transform.position.x + 20)
            {
                if (transform.position.y > ps.transform.position.y - 20 && transform.position.y < ps.transform.position.y + 20)
                {
                    transform.position = Vector3.MoveTowards(transform.position, ps.transform.position, 25f * Time.deltaTime);
                }
            }
        }
	}

	void OnCollisionEnter(Collision col){
		if(col.gameObject.name == "PlayerContainer"){
            col.gameObject.GetComponent<Player>().PlayerHP += 1;

            GameObject L = Resources.Load("effects/HPpickup") as GameObject;
            GameObject I = Instantiate(L) as GameObject;

            I.transform.position = transform.position;

			Destroy (gameObject);
		}
	}
}
