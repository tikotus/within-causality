﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScattershotImpact : MonoBehaviour {

	float removeWait;

	// Use this for initialization
	void Start () {
		removeWait = 3f;
	}
	
	// Update is called once per frame
	void Update () {


		gameObject.transform.Translate (Vector3.back * Random.Range (20, 141) * Time.deltaTime);
		gameObject.transform.Translate (Vector3.forward * Random.Range (20, 141) * Time.deltaTime);
		gameObject.transform.Translate (Vector3.up * Random.Range (20, 141) * Time.deltaTime);
		gameObject.transform.Translate (Vector3.down * Random.Range (20, 141) * Time.deltaTime);
		gameObject.transform.Translate (Vector3.left * Random.Range (20, 141) * Time.deltaTime);
		gameObject.transform.Translate (Vector3.right * Random.Range (20, 141) * Time.deltaTime);

		removeWait -= Time.deltaTime;
		if(removeWait <= 0){
			Destroy (gameObject);
		}
	}
}
