﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour {

	float velocity = -1f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		gameObject.transform.position += new Vector3 (0,0,velocity);
		if(gameObject.transform.position.z < -60){
			Destroy (gameObject);
			Destroy (this);
		}
		velocity -= Time.deltaTime;
	}
}
